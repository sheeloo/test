const fs = require("fs");


const output_file_path = "./src/public/output/";

const matchesPlayedPerYear = require("../ipl/matchesPlayedPerYear.js");
const matchesWonPerTeamPerYear = require("../ipl/matchesWonPerTeamPerYear.js");
const extraRunsConcededPerTeamIn2016 = require("../ipl/extraRunsConcededPerTeamIn2016.js");
const top10EconomicalBowlersIn2015 = require("../ipl/top10EconomicalBowlersIn2015.js");

const csvtojsonConverter = require("../ipl/csvtojsonConverter.js");


async function getData() {

    const matches = await csvtojsonConverter('matches.csv');
    const deliveries = await csvtojsonConverter('deliveries.csv');

    let result = ipl(matches, deliveries);

    save_JSON_files(result);
}

getData();


function ipl(matches, deliveries) {
    let result = {};
    result.matchesPlayedPerYear = matchesPlayedPerYear(matches);
    result.matchesWonPerTeamPerYear = matchesWonPerTeamPerYear(matches);
    result.extraRunsConcededPerTeamIn2016 = extraRunsConcededPerTeamIn2016(deliveries, matches);
    result.top10EconomicalBowlersIn2015 = top10EconomicalBowlersIn2015(deliveries, matches);
    return result;
}


function save_JSON_files(result) {

    for (let i in result) {
        console.log(result[i]);
        let jsonString = JSON.stringify(result[i]);
        fs.writeFile(output_file_path + i, jsonString, "utf8", err => {
            if (err) {
                console.error(err);
            }
        });
    }


}